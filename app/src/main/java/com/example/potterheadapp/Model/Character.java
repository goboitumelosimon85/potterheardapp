package com.example.potterheadapp.Model;

public class Character {
    private String name;
    private String house;
    private String patronus;
    private String spacies;
    private String bloodStatus;
    private String role;
    private String school;
    private boolean deathEater;
    private boolean dumbledoresArmy;
    private boolean orderOfThePhoenix;
    private boolean ministryOfMagic;
    private String alias;
    private String wond;
    private String boggart;
    private String animagus;

    public Character(String name, String house, String patronus, String spacies, String bloodStatus, String role, String school, boolean deathEater, boolean dumbledoresArmy, boolean orderOfThePhoenix, boolean ministryOfMagic, String alias, String wond, String boggart, String animagus) {
        this.name = name;
        this.house = house;
        this.patronus = patronus;
        this.spacies = spacies;
        this.bloodStatus = bloodStatus;
        this.role = role;
        this.school = school;
        this.deathEater = deathEater;
        this.dumbledoresArmy = dumbledoresArmy;
        this.orderOfThePhoenix = orderOfThePhoenix;
        this.ministryOfMagic = ministryOfMagic;
        this.alias = alias;
        this.wond = wond;
        this.boggart = boggart;
        this.animagus = animagus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getPatronus() {
        return patronus;
    }

    public void setPatronus(String patronus) {
        this.patronus = patronus;
    }

    public String getSpacies() {
        return spacies;
    }

    public void setSpacies(String spacies) {
        this.spacies = spacies;
    }

    public String getBloodStatus() {
        return bloodStatus;
    }

    public void setBloodStatus(String bloodStatus) {
        this.bloodStatus = bloodStatus;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public boolean isDeathEater() {
        return deathEater;
    }

    public void setDeathEater(boolean deathEater) {
        this.deathEater = deathEater;
    }

    public boolean isDumbledoresArmy() {
        return dumbledoresArmy;
    }

    public void setDumbledoresArmy(boolean dumbledoresArmy) {
        this.dumbledoresArmy = dumbledoresArmy;
    }

    public boolean isOrderOfThePhoenix() {
        return orderOfThePhoenix;
    }

    public void setOrderOfThePhoenix(boolean orderOfThePhoenix) {
        this.orderOfThePhoenix = orderOfThePhoenix;
    }

    public boolean isMinistryOfMagic() {
        return ministryOfMagic;
    }

    public void setMinistryOfMagic(boolean ministryOfMagic) {
        this.ministryOfMagic = ministryOfMagic;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getWond() {
        return wond;
    }

    public void setWond(String wond) {
        this.wond = wond;
    }

    public String getBoggart() {
        return boggart;
    }

    public void setBoggart(String boggart) {
        this.boggart = boggart;
    }

    public String getAnimagus() {
        return animagus;
    }

    public void setAnimagus(String animagus) {
        this.animagus = animagus;
    }
}

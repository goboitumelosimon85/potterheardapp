package com.example.potterheadapp.Interface;


import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PotterHeadApi {
    // Api key requered for characters
    @GET("characters?key=$2a$10$1JEnmtEF417yBaFZcr51qukRjaKv8d5toEG5DKP/IUZWIVwfsaF7y")
    Call<List<Character>> getCharacters();
}
